variables:
  GIT_DEPTH: 100
  GIT_SUBMODULE_STRATEGY: normal

stages:
 - build
 - test
 - deploy

.test:
  interruptible: true
  artifacts:
    expire_in: 2 weeks
    when: always
    paths:
    - "*.tar.gz"
    - ./*.log
    - ./config.h
    - ./**/*.log
    - clang-analyzer
    - doc/reference/html
    - doc/reference/*.pdf
    - doc/manual
    - coverage
    - cyclo

Debian:
  extends: .test
  image: debian:bullseye-slim
  stage: build
  before_script:
  - apt-get update -qq | tail -10
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq git autoconf automake libtool autopoint gettext make gcc valgrind libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev gtk-doc-tools texinfo bison gengetopt help2man libglib2.0-dev indent libtasn1-bin
  script:
  - ./bootstrap --skip-po
  - ./configure --enable-gcc-warnings --disable-silent-rules
  - make syntax-check
  - make check
  - make dist
  - git diff --exit-code # nothing should change version controlled files

Ubuntu-22.04:
  extends: .test
  image: ubuntu:22.04
  stage: build
  before_script:
  - apt-get update -qq | tail -10
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq git autoconf automake libtool autopoint gettext make gcc valgrind libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev gtk-doc-tools texinfo bison gengetopt help2man libglib2.0-dev indent libtasn1-bin
  script:
  - ./bootstrap --skip-po
  - ./configure --enable-gcc-warnings --disable-silent-rules
  - make syntax-check check
  - make dist
  - git diff --exit-code # nothing should change version controlled files
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q texlive texlive-plain-generic texlive-extra-utils gpg gpgv2
  - gpg --batch --passphrase '' --quick-gen-key bug-shishi@gnu.org
  - git checkout -b fake-release
  - git config user.email "bug-shishi@gnu.org"
  - git config user.name "GNU Shishi CICD Builder"
  - git config user.signingkey bug-shishi@gnu.org
  - sed -i '5i ** WARNING This release was prepared automatically with no testing.\n' NEWS
  - git commit -m "Warn about automatic release." NEWS
  - make release-commit RELEASE='17.42.23 stable'
  - make
  - make release RELEASE='17.42.23 stable'

AlmaLinux8:
  extends: .test
  image: almalinux:8
  stage: build
  before_script:
  - dnf -y update
  - dnf -y install epel-release
  - dnf --enablerepo=powertools -y install git make diffutils file hostname patch autoconf automake libtool gettext-devel texinfo help2man gengetopt libgcrypt-devel libtasn1-devel libtasn1-tools bison
  script:
  - ./bootstrap --skip-po
  - ./configure --enable-gcc-warnings --disable-silent-rules
  - make check V=1 VERBOSE=t
  - make syntax-check
  - make install
  - git diff --exit-code # nothing should change version controlled files

Ubuntu-coverage-cyclo:
  extends: .test
  image: ubuntu:rolling
  stage: build
  before_script:
  - apt-get update -qq
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq git autoconf automake libtool autopoint gettext make texinfo texlive texlive-plain-generic texlive-extra-utils texlive-font-utils help2man gtk-doc-tools libglib2.0-dev valgrind gengetopt wget libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev libtasn1-bin bison
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -qq lcov pmccabe
  script:
  - ./bootstrap
  - ./configure --enable-gcc-warnings --disable-silent-rules CFLAGS="-g --coverage"
  - lcov --directory . --zerocounters
  - make check VERBOSE=t
  - make dist
  - make syntax-check
  - mkdir coverage
  - lcov --directory . --output-file coverage/shishi.info --capture
  - lcov --remove coverage/shishi.info '/usr/include/*' '*/gl/*' '*/gltests/*' '*/lib/gl/*' '*/lib/gltests/*' -o coverage/shishi_filtered.info
  - genhtml --output-directory coverage coverage/shishi_filtered.info --highlight --frames --legend --title "GNU Shishi"
  - mkdir cyclo
  - make -C doc/cyclo/ cyclo-shishi.html
  - cp -v doc/cyclo/cyclo-shishi.html cyclo/index.html
  - git diff --exit-code # nothing should change version controlled files

Fedora-clanganalyzer:
  extends: .test
  image: fedora:latest
  stage: build
  before_script:
  - dnf update -y
  - dnf install -y git make diffutils file hostname patch autoconf automake libtool gettext-devel texinfo texinfo-tex texlive texlive-supertabular texlive-framed texlive-morefloats texlive-quotchap texlive-epstopdf help2man gtk-doc gengetopt gperf wget dblatex libgcrypt-devel libtasn1-devel libtasn1-tools bison
  - dnf install -y clang clang-analyzer
  script:
  - ./bootstrap
  - scan-build ./configure --disable-silent-rules --enable-gtk-doc --enable-gtk-doc-pdf
  - scan-build -o clang-analyzer make V=1
  - make web-manual
  - make dist
  - git diff --exit-code # nothing should change version controlled files

.pages:
  stage: deploy
  needs: ["Ubuntu-coverage-cyclo", "Fedora-clanganalyzer"]
  script:
    - mkdir public
    - mv coverage/ cyclo/ public/
    - mv clang-analyzer/* public/clang-analyzer
    - mv doc/manual public/manual
    - mv doc/reference/html public/reference
    - mv doc/reference/shishi.pdf public/reference/
  artifacts:
    paths:
    - public
    expire_in: 30 days

pages:
  extends: .pages
  only:
    - master

pages-test:
  extends: .pages
  except:
    - master

Ubuntu-14.04:
  extends: .test
  image: ubuntu:14.04
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc valgrind libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev
  script:
  - tar xfa shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t

Ubuntu-20.04:
  extends: .test
  image: ubuntu:20.04
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc valgrind libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev
  script:
  - tar xfa shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t

Ubuntu-distcheck:
  extends: .test
  image: ubuntu:devel
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc gtk-doc-tools texinfo texlive texlive-plain-generic valgrind libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev
  script:
  - tar xfa shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check distcheck V=1 VERBOSE=t

CentOS7:
  extends: .test
  image: centos:7
  stage: test
  needs: [Debian]
  before_script:
  - yum -y install make gcc diffutils libgcrypt-devel libtasn1-devel libtasn1-tools
  script:
  - tar xfa shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t

Alpine:
  extends: .test
  image: alpine:latest
  stage: test
  needs: [Debian]
  before_script:
  - echo "ipv6" >> /etc/modules
  - apk update
  - apk add build-base gettext gettext-dev libgcrypt-dev libtasn1-dev
  script:
  - tar xfz shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t

ArchLinux:
  extends: .test
  image: archlinux:latest
  stage: test
  needs: [Debian]
  before_script:
  - pacman -Syu --noconfirm make gcc diffutils file
  script:
  - tar xfz shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - mkdir b
  - cd b
  - ../configure
  - make check V=1 VERBOSE=t

Debian8:
  extends: .test
  image: debian:stretch-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc indent libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev
  script:
  - tar xfz shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t
  - make syntax-check

Debian9:
  extends: .test
  image: debian:buster-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev
  script:
  - tar xfz shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --enable-gcc-warnings
  - make check V=1 VERBOSE=t

Debian10:
  extends: .test
  image: debian:stretch-slim
  stage: test
  needs: [Debian]
  before_script:
  - apt-get update -q
  - env DEBIAN_FRONTEND=noninteractive apt-get install -y -q make gcc libgcrypt-dev libgnutls28-dev libidn11-dev libpam0g-dev libtasn1-6-dev
  script:
  - tar xfz shishi-*.tar.gz
  - cd `ls -d shishi-* | grep -v tar.gz`
  - ./configure --disable-gcc-warnings
  - make check V=1 VERBOSE=t
